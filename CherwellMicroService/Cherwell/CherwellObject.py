import json, requests
import logging


class Cherwell(object):
    """
    Class for generating/updating/deleting Cherwell Incidents Business Objects
    """

    """
    Class Private Attributes
    """
    __api_key           = ""
    __username          = ""
    __password          = ""
    __url               = ""
    __token             = ""

    __busObId           = ""
    __error_message     = ""
    __bus_ob_name       = ""
    __headers           = {}
    __data              = {}

    """
    Private class methods
    """

    def __login(self):
        """
        Login to Cherwell API
        :param self: Initalized Incident Object
        :return: API Bearer Token
        """

        log = logging.getLogger("test.test")
        token_uri = self.__url + "token"

        token_request_body = {
            "Accept": "application/json",
            "grant_type": "password",
            "client_id": self.__api_key,
            "username": self.__username,
            "password": self.__password,
        }

        request_uri = token_uri + "?auth_mode=%s&api_key=%s" % ("Internal", self.__api_key)
        response = requests.request("POST", request_uri, data=token_request_body)
        return response

    def __get_bo_summary(self, business_object_name):
        """
        Get the Business Object summary and id
        :param self: Initalized Business Object
        :return: GET response from Cherwell
        """
        summary_uri = self.__url + "api/V1/getbusinessobjectsummary/busobname/" + business_object_name

        response = requests.request("GET", summary_uri, headers=self.__headers)
        return response

    def __get_template(self):
        """
        Get Business Object Template
        param self: Initalized Business Object
        :return: incident business object
        """
        get_template_uri = self.__url + "api/V1/GetBusinessObjectTemplate"

        template_request = {
            "busObId": self.__busObId,
            "includeRequired": True,
            "includeAll": True,
        }

        template_request = json.dumps(template_request)

        response = requests.request("POST", get_template_uri, data=template_request, headers=self.__headers)
        return response

    def __save_bo(self, data):
        """
        Saves a Business Object
        :param data: JSON object representing business object to be saved
        :return: POST response from Cherwell
        """
        save_busob_uri = self.__url + "api/V1/SaveBusinessObject"

        response = requests.request("POST", save_busob_uri, data=json.dumps(data), headers=self.__headers)
        return response

    def __get_bo(self, boid, rec_id):
        """
        Get a business object by id
        :param boid: ID representing the incident table
        :param rec_id: ID representing the incident to be updated
        :return: POST response from Cherwell
        """

        incident_retrieval_uri = self.__url + "/api/V1/getbusinessobject/busobid/%s/busobrecid/%s" % (boid, rec_id)
        response               = requests.request("GET", url=incident_retrieval_uri, headers=self.__headers)

        return response

    def __search_bo(self, search_request):
        """
        Search for a business object
        :param search_request: JSON object representing the search query to execute
        :return: POST response from Cherwell
        """
        search_uri = self.__url + "/api/V1/getsearchresults"
        response   = requests.request("POST", url=search_uri, data=json.dumps(search_request), headers=self.__headers)

        return response

    """
    Public class methods
    """

    def create_business_object(self, data):
        """
        Create new Business Object
        :param data: JSON object representing business object
        :return: Dict {busObId, busObRec}
        """
        template_response = self.__get_template()
        template_fields   = json.loads(template_response.text)
        if template_fields is None or template_response.status_code < 200 or template_response.status_code >= 300:
            raise ValueError("ERROR: Unable to get Business Object Template - %s" % template_response.text)

        for field in template_fields['fields']:
            if field['name'] in data.keys():
                field['value'] = data[field['name']]
                field['dirty'] = True

        create_request = {
            "busObId": self.__busObId,
            "fields":  template_fields['fields'],
        }

        create_response = self.__save_bo(create_request)
        if create_response is None or create_response.status_code < 200 or create_response.status_code >= 300:
            raise ValueError("ERROR: Unable to create Incident - %s" % create_response.text)

        create_response = json.loads(create_response.text)
        return {'busObPubId':create_response['busObPublicId'], 'busObRecId':create_response['busObRecId']}

    def update_business_object(self, bo_rec_id, bo_public_id, update):
        """
        Update Cherwell Business Object
        :param bo_rec_id: record id for the object to update
        :param bo_public_id: public id for the object to update
        :param update:       fields to update for the target object
        :return: response text from the api
        """

        template_response = self.__get_template()
        template_fields   = json.loads(template_response.text)
        if template_fields is None or template_response.status_code < 200 or template_response.status_code >= 300:
            raise ValueError("ERROR: Unable to get Business Object Template - %s" % template_response.text)

        for field in template_fields['fields']:
            if field['name'] in update.keys():
                field['value'] = update[field['name']]
                field['dirty'] = True

        update_request = {
            "busObId":       self.__busObId,
            "busObPublicId": bo_public_id,
            "busObRecId":    bo_rec_id,
            "fields":  template_fields['fields'],
        }
        print(update_request)

        update_response = self.__save_bo(update_request)
        if update_response is None or update_response.status_code < 200 or update_response.status_code >= 300:
            raise ValueError("ERROR: Unable to update Incident - %s" % update_response.text)

        return update_response.text

    def search_business_objects(self, search_name, search_filters):
        """
        Search Cherwell Business Objects
        :param search_name: name of the saved search to execute
        :param search_filters: JSON object of search filters
        :return: POST response from Cherwell
        """

        template_response = self.__get_template()
        template_fields   = json.loads(template_response.text)
        if template_fields is None or template_response.status_code < 200 or template_response.status_code >= 300:
            raise ValueError("ERROR: Unable to get Business Object Template - %s" % template_response.text)

        # array to hold our unique fields
        unique_fields = []

        # we will replace all instances of a field being referenced by it's name with it's underlying ID
        # by referencing the incident template
        number_of_filters_updated = 0
        for field in template_fields['fields']:
            for search_filter in search_filters:
                if search_filter['fieldId'] == field['name']:
                    # a requirement for the cherwell api, needs a list of unique field ids searched,
                    # filters aren't enough
                    if field['fieldId'] not in unique_fields:
                        unique_fields.append(field['fieldId'])

                    filter_identifier = """BO:{0},FI:{1}""".format(self.__busObId,
                                                                   field['fieldId'])
                    search_filter['fieldId'] = filter_identifier
                    number_of_filters_updated += 1

        # if we were unable to replace all field_names, throw an error, i.e. the user has requested for a field
        # not in the Cherwell incident template
        if len(search_filters) < number_of_filters_updated:
            raise ValueError("ERROR: User has requested to search one, or more fields that do not exist")

        # depending on the number of results, we need to keep an index to incrementally retrieve more results
        # from Cherwell since Cherwell only returns 1000 results at a time
        return_search_blob = {'search_results':[]}
        page_counter       = 0
        while True:
            search_request = {
                "busObID" :        self.__busObId,
                "association":     self.__bus_ob_name,
                "searchName":      search_name,
                "fields":          unique_fields,
                "scope":           "Global",
                "scopeOwner":      "(None)",
                "includeAllFields":"false",
                "includeRequired": "true",
                "pageSize":        "1000",
                "pageNumber":      page_counter,
                "filters":         search_filters
            }

            # validate the search response
            response = self.__search_bo(search_request)
            if response is None or response.status_code < 200 or response.status_code >= 300:
                raise ValueError("ERROR: unable to issue search query - %s" % response.text)

            # convert to json object, and append it to our search array
            json_parsed_response = json.loads(response.text)
            return_search_blob['search_results'].append(json_parsed_response)
            if len(json_parsed_response['businessObjects']) >= 1000:

                # if we recieved more than 1000 results, up the page counter, if we have to increase
                # past '5' we have returned more than 5000 results from the query, and need to fail
                page_counter += 1
                if page_counter > 5:
                    raise ValueError("ERROR: More then 5000 results returned. Consider revising query")
            else:
                break

        return return_search_blob

    """
    Dunder methods
    """

    def __init__(self, bo_name, url, token, username, password, api_key):
        """
        Constructor for instatiating the class
        :params: bo_name  - name of the business object you are interacting with in Cherwell
                 url      - host url for target Cherwell instance (string),
                 api_key  - key      for SA (string),
                 username - username for SA (string),
                 password - password for SA (string),
        :return: CherwellIncident Object
        """
        print("C")
        # set our private attributes
        self.__url         = "https://" + url + "/CherwellAPI/"
        self.__api_key     = api_key
        self.__username    = username
        self.__password    = password
        self.__bus_ob_name = bo_name

        # acquire a token
        """
        token_response = ""
        token_response = self.__login()
        self.__token = json.loads(token_response.content)['access_token']
        if self.__token is "" or token_response.status_code < 200 or token_response.status_code >= 300:
            raise ValueError("ERROR: Unable to gather Bearer Token -  %s" % token_response.text)
        """
        self.__token = token
        print("Test")
        print(self.__token)

        # set our header with the oAuth token
        self.__headers = {
            "Content-type": "application/json",
            "Authorization": "Bearer " + self.__token,
        }

        # get the business object id
        response = self.__get_bo(bo_name)
        self.__busObId    = json.loads(response.content)[0]['busObId']
        if self.__busObId is None or response.status_code < 200 or response.status_code >= 300:
            raise ValueError("ERROR: Unable to get Business Object ID - %s" % response.text)

    def __del__(self):
        """
        Delete Token
        :param   Self Pointer, void
        :returns void

        delete_token_uri = self.__url + "api/V1/logout"
        response = requests.request("DELETE", delete_token_uri, headers=self.__headers)
        """
        pass

    def __repr__(self):
        """
        Repr 
        """
        return """Cherwell Incident Object:
                  API key : {0}
                  Username: {1}
                  Url     : {2}
                  busObId : {3}
               """.format(self.__api_key, 
                          self.__username, 
                          self.__url, 
                          self.__busObId)