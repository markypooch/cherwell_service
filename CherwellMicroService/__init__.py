from flask import app
from prometheus_flask_exporter import PrometheusMetrics
import connexion

app     = connexion.App(__name__, specification_dir='./')
token   = ""