from   flask                                           import request, Response, json
from   CherwellMicroService.Cherwell.Cherwell          import Cherwell
from   prometheus_client                               import *
from   CherwellMicroService                            import *
import requests, datetime

# comment
CREATE_BO_TIME = Summary('create_bo_request_processing_seconds',
                         'DESC: INDEX time spent processing create_incident request')
SEARCH_BO_TIME = Summary('search_bo_request_processing_seconds',
                         'DESC: INDEX time spent processing search_incidents request')
UPDATE_BO_TIME = Summary('update_bo_request_processing_seconds',
                         'DESC: INDEX time spent processing update_incident request')


@app.route('/api/session', methods=['POST', 'DELETE'])
def start_session():
    global token
    response = None
    try:
        token_request_body = json.loads(request.data)
        token_request = {
            "Accept": "application/json",
            "grant_type": "password",
            "client_id": token_request_body['api_key'],
            "username": token_request_body['username'],
            "password": token_request_body['password']
        }

        token_uri   = "https://"+token_request_body['url']+"/CherwellAPI/token"
        request_uri = token_uri + "?auth_mode=%s&api_key=%s" % ("Internal", token_request_body['api_key'])

        response = requests.request("POST", request_uri, data=token_request)
        if response.status_code >= 200 and response.status_code < 300:
            token = json.loads(response.content)['access_token']
        else:
            return Response(json.dumps({"exception":str(response.content)}),
                            status=response.status_code, mimetype='application/json')
    except Exception as e:
        return Response(json.dumps({"exception":str(e)}), status=400, mimetype='application/json')
    return Response(json.dumps({"Message":"Successful"}), status=200, mimetype='application/json')


@app.route('/api/create_business_object', methods=['POST'])
@CREATE_BO_TIME.time()
def create_bo():
    global token
    cherwell_dict = {}
    try:
        request_payload                         = json.loads(request.data)

        # attempt to opt-out early if top-level attributes missing
        enforced_attributes                     = ('url','username','password','api_key','data')
        response = validate_attributes(enforced_attributes, request_payload)
        if response != "":
            return Response(json.dumps({"exception":str(response)}),
                            status=400,
                            mimetype='application/json')

        cherwell_obj    = Cherwell(request_payload['data']['type'],
                                   request_payload['url'],
                                   token,
                                   request_payload['username'],
                                   request_payload['password'],
                                   request_payload['api_key'])
        cherwell_dict   = cherwell_obj.create_business_object(request_payload['data'])

    except Exception as e:
        return Response(json.dumps({"exception":str(e)}), status=400, mimetype='application/json')
    return json.dumps(cherwell_dict)


@app.route('/api/update_business_object', methods=['POST'])
@UPDATE_BO_TIME.time()
def update_bo():
    global token
    response = ""
    try:
        request_payload = json.loads(request.data)

        # attempt to opt-out early if top-level attributes missing
        enforced_attributes = ('url','username','password','api_key','incident')
        response = validate_attributes(enforced_attributes, request_payload)
        if response != "":
            return Response(json.dumps({"exception":str(response)}),
                            status=400,
                            mimetype='application/json')

        cherwell_obj    = Cherwell(request_payload['data']['type'],
                                   request_payload['url'],
                                   token,
                                   request_payload['username'],
                                   request_payload['password'],
                                   request_payload['api_key'])
        response        = cherwell_obj.update_business_object(request_payload['data']['record_id'],
                                                              request_payload['data']['public_id'],
                                                              request_payload['data']['type'],
                                                              request_payload['data']['fields'])
    except Exception as e:
        return Response(json.dumps({"exception":str(e)}), status=400, mimetype='application/json')
    return response


@app.route('/api/search_business_objects', methods=['POST'])
@SEARCH_BO_TIME.time()
def search_bo():
    global token
    search_results = None
    try:
        request_payload = json.loads(request.data)

        # attempt to opt-out early if top-level attributes missing
        enforced_attributes = ('url','username','password','api_key','search_parameters')
        response = validate_attributes(enforced_attributes, request_payload)
        if response != "":
            return Response(json.dumps({"exception":str(response)}),
                            status=400,
                            mimetype='application/json')

        cherwell_obj    = Cherwell(request_payload['data']['type'],
                                   request_payload['url'],
                                   token,
                                   request_payload['username'],
                                   request_payload['password'],
                                   request_payload['api_key'])

        search_results  = cherwell_obj.search_business_objects(request_payload['search_parameters']['search_name'],
                                                               request_payload['search_parameters']['search_filters'])
    except Exception as e:
        return Response(json.dumps({"exception":str(e)}), status=400, mimetype='application/json')
    return json.dumps(search_results)


@app.route('/api/get_business_object', methods=['GET'])
def search_bo():
    try:
        request_payload = json.loads(request.data)

        # attempt to opt-out early if top-level attributes missing
        enforced_attributes = ('url','username','password','api_key','search_parameters')
        response = validate_attributes(enforced_attributes, request_payload)
        if response != "":
            return Response(json.dumps({"exception":str(response)}),
                            status=400,
                            mimetype='application/json')

        cherwell_obj    = Cherwell(request_payload['data']['type'],
                                   request_payload['url'],
                                           token,
                                           request_payload['username'],
                                           request_payload['password'],
                                           request_payload['api_key'])

        search_results  = cherwell_obj.search_business_objects(request_payload['search_parameters']['search_name'],
                                                               request_payload['search_parameters']['search_filters'])
    except Exception as e:
        return Response(json.dumps({"exception":str(e)}), status=400, mimetype='application/json')
    return json.dumps(search_results)


@app.route('/api/alert_payloads', methods=['POST', 'GET'])
def post_alerts():
    try:
        request_payload = json.loads(request.data)
        cherwell_obj    = Cherwell(request_payload['data']['type'],
                                   request_payload['url'],
                                           token,
                                           request_payload['username'],
                                           request_payload['password'],
                                           request_payload['api_key'])

        for alert in request_payload['alerts']:
            alert_search_name = alert['alertId'] + "_" + str(datetime.datetime.now().timestamp())

            search_results  = cherwell_obj.search_alerts(alert_search_name)

    except Exception as e:
        return Response(json.dumps({"exception":str(e)}), status=400, mimetype='application/json')
    return response


def validate_attributes(attributes, request_payload):
    return next((str("Missing " + attr) for attr in attributes if attr not in request_payload), "")
