FROM python:3.6

WORKDIR /
COPY    . /

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "cherwell_micro_service.py"]