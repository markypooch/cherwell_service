# Cherwell

A re-usable object for creating/removing/updating incidents.
As for right now, to use, just throw the `CherwellIncidents.py` file into your 
project directory in order to use.

the dependencies are captured in `requirements.txt` if you are leveraging
pip

## Documentation

For documentation, please reference the `Tests/` directory for intended usage
of the object.