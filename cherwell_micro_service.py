from   flask                                       import Flask, Response, request, json, jsonify 
from   prometheus_client                           import start_http_server, Summary
from   CherwellMicroService.micro_services         import *
import connexion

app.add_api('./Documentation/swagger.yml')

if __name__ == "__main__":
    start_http_server(8000)
    app.run(debug=False, host="0.0.0.0")